package com.liqiang.mvpsample.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;

import static com.liqiang.mvpsample.view.Main2Activity.getContext;

/**
 * Created by liqiang on 2018/8/9.
 */

public class Main2ActivityTool {
    public class LooperThread extends Thread {
        Handler handler;

        @SuppressLint("HandlerLeak")
        @Override
        public void run() {
            super.run();
            Looper.prepare();
            handler = new Handler() {

                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    final int arg1 = msg.arg1;
                    new Activity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getContext(),"主线程发送了 ："+String.valueOf(arg1), Toast.LENGTH_LONG).show();

                        }
                    });
                }

            };
            Looper.loop();

        }

    }
}
