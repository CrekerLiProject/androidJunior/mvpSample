package com.liqiang.mvpsample.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.liqiang.mvpsample.R;

/**
 * Created by liqiang on 2018/8/7.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
