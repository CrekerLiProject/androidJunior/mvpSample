package com.liqiang.mvpsample.view;

/**
 * Created by liqiang on 2018/8/7.
 */

public interface IWeatherView {
    /**
     * 更新数据
     * @param info 待显示的数据
     */
    void onInfoUpdate(String info);

    /**
     * 显示等待框
     */
    void showWaitingDialog();

    /**
     * 停止等待框
     */

    void dismissWaitingDialog();
}
