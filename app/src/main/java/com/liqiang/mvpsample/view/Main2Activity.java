package com.liqiang.mvpsample.view;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;

import com.liqiang.mvpsample.R;

public class Main2Activity extends BaseActivity {
    Button button;
    private static Context context;
    private Main2ActivityTool main2ActivityTool = new Main2ActivityTool();
    private final Main2ActivityTool.LooperThread looperThread = main2ActivityTool.new LooperThread();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        button=findViewById(R.id.btn_main_2_child);
        context=Main2Activity.this;
        looperThread.start();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message message = Message.obtain();
                message.arg1 = 3;
                looperThread.handler.sendMessage(message);
            }
        });
    }
    public static Context getContext(){
        return context;
    }




}
