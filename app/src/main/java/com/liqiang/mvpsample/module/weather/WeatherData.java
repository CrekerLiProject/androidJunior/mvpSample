package com.liqiang.mvpsample.module.weather;

/**
 * Created by liqiang on 2018/8/7.
 */

public class WeatherData implements WeatherModule{
    private String data = null;
    @Override
    public String get() {
        return data;
    }

    @Override
    public void set(String s) {
        data = s;
    }
}
