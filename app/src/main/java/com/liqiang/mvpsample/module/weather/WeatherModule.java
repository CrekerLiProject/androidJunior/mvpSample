package com.liqiang.mvpsample.module.weather;

/**天气接口规范
 * Created by liqiang on 2018/8/7.
 */

public interface WeatherModule {
    String get();
    void set(String s);
}
