
package com.liqiang.mvpsample.presenter.weather;

import com.liqiang.mvpsample.module.weather.WeatherData;
import com.liqiang.mvpsample.module.weather.WeatherModule;
import com.liqiang.mvpsample.view.IWeatherView;

public class WeatherPresenter {
    WeatherModule mModel;
    IWeatherView mView;

    public WeatherPresenter(IWeatherView mView) {
        this.mView = mView;
        mModel = new WeatherData();
    }

    /**
     * 获取天气信息，供外部调用
     */
    public void requestWetherInfo(){
        getNetworkInfo();
    }
    //和VIEW层的交互（二次封装VIEW的重写方法）
    private void showWaitingDialog(){
        if (mView != null) {
            mView.showWaitingDialog();
        }
    }

    private void dissmissWaitingDialog(){
        if (mView != null) {
            mView.dismissWaitingDialog();
        }
    }

    private void updateWetherInfo(String info){
        if (mView != null) {
            mView.onInfoUpdate(info);
        }
    }
    //和MODULE层的交互（二次封装MODULE层的方法）
    private void saveInfo(String info){
        mModel.set(info);
    }

    private String localInfo(){
        return mModel.get();
    }

    private void getNetworkInfo(){
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    //打开等待对话框
                    showWaitingDialog();
                    //模拟网络耗时
                    Thread.sleep(6000);

                    String info = "21度，晴转多云";
                    //保存到Model层
                    saveInfo(info);
                    //从Model层获取数据，为了演示效果，实际开发中根据情况需要。
                    String localinfo = localInfo();

                    //通知View层改变视图
                    updateWetherInfo(localinfo);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    //取消对话框
                    dissmissWaitingDialog();
                }
            }
        }).start();
    }
}